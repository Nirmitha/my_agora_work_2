//
//  ViewController.swift
//  VideoCallApp
//
//  Created by dasun on 2022-03-23.
//

import UIKit
import AgoraRtcKit

class ViewController: UIViewController {
    // The video feed for the local user is displayed here
    var localView: UIView!
    // The video feed for the remote user is displayed here
    var remoteView: UIView!
    // Click to join or leave a call
    var joinButton: UIButton!
    // Track if the local user is in a call
    var joined: Bool = false
    // The main entry point for Video SDK
    var agoraEngine: AgoraRtcEngineKit!
    // By default, the current user sends video content
    var userRole: AgoraClientRole = .broadcaster
    // Update with the App ID of your project generated on Agora Console.
    var appID = "906b5e2c9f0545379f3885699d82eabc"
    // Update with the temporary token generated in Agora Console.
    //TODO: Update with the latest token
    var token = "006906b5e2c9f0545379f3885699d82eabcIAARBbGVpPWtLI2CtIu22BH0InBu68z3+hklOZiSXKcsooup3jAAAAAAEACDdLwQ92haYgEAAQD3aFpi"
    // Update with the channel name you used to generate the token in Agora Console.
    var channelName = "channel_dasun"

    // Attach volume change related UI elements as IBOutlets
    @IBOutlet weak var volumeSliderLabel: UILabel!
    @IBOutlet weak var volumeSlider: UISlider!
    
    // Add pre-call tests related UI elements as IBOutlets
    var echoTestBtn: UIButton!
    var probeTestBtn: UIButton!
    
    var echoTestIsRunning:Bool = false
    var probeTestIsRunning:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Initializes the video view
        initViews()
        // The following functions are used when calling Agora APIs
        initializeAgoraEngine()
        
        // Set initial volume-related values and restrictions
        volumeSlider.maximumValue = 100
        volumeSlider.value = 80
        agoraEngine.adjustPlaybackSignalVolume(80)
    }
    
    @IBAction func volumeSliderValueChanged(_ sender: UISlider) {
        let value:Int = Int(sender.value)
        print("Changing volume to \(value)")
        agoraEngine.adjustPlaybackSignalVolume(value)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        leaveChannel()
        AgoraRtcEngineKit.destroy()
    }

    func joinChannel() {

        if !self.checkForPermissions() {
            return
        }
        agoraEngine.setChannelProfile(.liveBroadcasting)

        if self.userRole == .broadcaster {
            agoraEngine.setClientRole(self.userRole)
            setupLocalVideo()
        }

        agoraEngine.setDefaultAudioRouteToSpeakerphone(true)
        // Join the channel with a token. Pass in your token and channel name here
        agoraEngine.joinChannel(
            byToken: token, channelId: channelName, info: nil, uid: 0,
            joinSuccess: { (channel, uid, elapsed) in }
        )
        joined = true
    }

    func leaveChannel() {
        agoraEngine.stopPreview()
        agoraEngine.leaveChannel(nil)
        joined = false
    }
    
    func initializeAgoraEngine() {
        // Use AgoraRtcEngineDelegate for the following delegate parameter.
        agoraEngine = AgoraRtcEngineKit.sharedEngine(withAppId: appID, delegate: self)
        // Enable the video module
        agoraEngine.enableVideo()
    }
    
    func setupLocalVideo() {
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = 0
        videoCanvas.renderMode = .hidden
        videoCanvas.view = localView

        // Sets the local video view
        agoraEngine.setupLocalVideo(videoCanvas)
        // Starts the local video preview
        agoraEngine.startPreview()
    }

   override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        remoteView.frame = CGRect(x: 20, y: 50, width: 350, height: 330)
        localView.frame = CGRect(x: 20, y: 400, width: 350, height: 330)
    }

    func initViews() {
        remoteView = UIView()
        self.view.addSubview(remoteView)
        localView = UIView()
        self.view.addSubview(localView)
        //  Button to join or leave a channel
        joinButton = UIButton(type: .system) // let preferred over var here
        joinButton.frame = CGRect(x: 140, y:700, width:100, height:50)
        joinButton.setTitle("Join", for: .normal)

        joinButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(joinButton)
        
        // Setting initial size and value configurations for pre-call test related buttons
        echoTestBtn = UIButton(type: .system)
        echoTestBtn.frame = CGRect(x: 120, y:600, width:150, height:50)
        echoTestBtn.setTitle("Run echo test", for: .normal)
        
        echoTestBtn.addTarget(self, action: #selector(runEchoTestBtnClicked), for: .touchUpInside)
        self.view.addSubview(echoTestBtn)
        
        probeTestBtn = UIButton(type: .system)
        probeTestBtn.frame = CGRect(x: 120, y:650, width:150, height:50)
        probeTestBtn.setTitle("Run probe test", for: .normal)
        
        probeTestBtn.addTarget(self, action: #selector(runProbeTestBtnClicked), for: .touchUpInside)
        self.view.addSubview(probeTestBtn)
    }

    @objc func buttonAction(sender: UIButton!) {
        if (!joined) {
            joinChannel()
            joinButton.setTitle("Leave", for: .normal)
        } else {
            leaveChannel()
            joinButton.setTitle("Join", for: .normal)
        }
    }
    
    // Add event responders for pre-call tests related UI elements
    @objc func runEchoTestBtnClicked(sender: UIButton!) {
        print("runEchoBtnClicked event fired")
        agoraEngine.setChannelProfile(.liveBroadcasting)
        agoraEngine.setClientRole(.broadcaster)
        
        if (echoTestIsRunning) {  // logic to stop the echo test
            agoraEngine.stopEchoTest()
            
            echoTestBtn.setTitle("Run echo test", for: .normal)
            echoTestIsRunning = false
        } else {  // logic to start the echo test
            let config = AgoraEchoTestConfiguration()
            config.channelId = channelName
            config.token = token
            config.view = localView
            config.enableAudio = true
            config.enableVideo = true
            
            agoraEngine.startEchoTest(withConfig: config)
            
            echoTestBtn.setTitle("Stop echo test", for: .normal)
            echoTestIsRunning = true
        }
    }

    @objc func runProbeTestBtnClicked(sender: UIButton!) {
        print("runProbeTestBtnClicked event fired")
        agoraEngine.setChannelProfile(.liveBroadcasting)
        agoraEngine.setClientRole(.broadcaster)

        if (probeTestIsRunning) { // logic to stop the probe test
            agoraEngine.stopLastmileProbeTest()

            probeTestBtn.setTitle("Run probe test", for: .normal)
            probeTestIsRunning = false
        } else { // logic to start the probe test
            let config = AgoraLastmileProbeConfig()
            config.probeUplink = true
            config.probeDownlink = true
            config.expectedUplinkBitrate = 100000  // range: [100000, 5000000]
            config.expectedDownlinkBitrate = 100000  // range: [100000, 5000000]

            agoraEngine.startLastmileProbeTest(config)

            probeTestBtn.setTitle("Stop probe test", for: .normal)
            probeTestIsRunning = true
        }
    }
    
    func checkForPermissions() -> Bool {
        var hasPermissions = false

        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: hasPermissions = true
            default: hasPermissions = requestCameraAccess()
        }
        // Break out, because camera permissions have been denied or restricted.
        if !hasPermissions { return false }
        switch AVCaptureDevice.authorizationStatus(for: .audio) {
            case .authorized: hasPermissions = true
            default: hasPermissions = requestAudioAccess()
        }
        return hasPermissions
    }

    func requestCameraAccess() -> Bool {
        var hasCameraPermission = false
        let semaphore = DispatchSemaphore(value: 0)
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
            hasCameraPermission = granted
            semaphore.signal()
        })
        semaphore.wait()
        return hasCameraPermission
    }

    func requestAudioAccess() -> Bool {
        var hasAudioPermission = false
        let semaphore = DispatchSemaphore(value: 0)
        AVCaptureDevice.requestAccess(for: .audio, completionHandler: { granted in
            hasAudioPermission = granted
            semaphore.signal()
        })
        semaphore.wait()
        return hasAudioPermission
    }

}

extension ViewController: AgoraRtcEngineDelegate {
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = uid
        videoCanvas.renderMode = .hidden
        videoCanvas.view = remoteView
        agoraEngine.setupRemoteVideo(videoCanvas)
    }
    
    // Add callbacks for pre-call probe test results.
    // Triggered 2 seconds after starting the last-mile test.
    func rtcEngine(_ engine: AgoraRtcEngineKit,
                   lastmileQuality quality: AgoraNetworkQuality) {
        var qualityDesc = ""
        switch quality.rawValue {
            case 0:
                qualityDesc = "The network quality is unknown."
            case 1:
                qualityDesc = "The network quality is excellent."
            case 2:
                qualityDesc = "The network quality is quite good, but the bitrate may be slightly lower than excellent."
            case 3:
                qualityDesc = "Users can feel the communication slightly impaired."
            case 4:
                qualityDesc = "Users can communicate only not very smoothly."
            case 5:
                qualityDesc = "The network quality is so bad that users can hardly communicate."
            case 6:
                qualityDesc = "The network is disconnected and users cannot communicate at all."
            case 7:
                qualityDesc = "Users cannot detect the network quality. (Not in use.)"
            case 8:
                qualityDesc = "Detecting the network quality."
            default:
                break
        }
        
        let qualityDescAlert = UIAlertController(title: "Network Quality Rating", message: qualityDesc, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        qualityDescAlert.addAction(okBtn)
        
        self.present(qualityDescAlert, animated: true, completion: nil)
    }
    
    // Triggered within 30 seconds of starting the last-mile test.
    func rtcEngine(_ engine: AgoraRtcEngineKit,
                   lastmileProbeTest result: AgoraLastmileProbeResult) {
        var stateDesc = ""
        switch result.state.rawValue {
            case 0:
                stateDesc = "The last-mile network probe test is complete."
            case 1:
                stateDesc = "The last-mile network probe test is incomplete and the bandwidth estimation is not available, probably due to limited test resources."
            case 2:
                stateDesc = "The last-mile network probe test is not carried out, probably due to poor network conditions."
            default:
                break
        }
        
        let rtt = "Round trip time (rtt): \(result.rtt)"
        let heading = [stateDesc, rtt].joined(separator: "\n")
        
        let uplinkBandwidth = "Available Uplink Bandwidth: \(result.uplinkReport.availableBandwidth)Kbps"
        let uplinkJitter = "Uplink Jitter: \(result.uplinkReport.jitter)ms"
        let uplinkLoss = "Uplink Packet Loss Rate: \(result.uplinkReport.packetLossRate)%"
        let uplinkReport = [uplinkBandwidth, uplinkJitter, uplinkLoss].joined(separator: "\n")
        
        let downlinkBandwidth = "Available Downlink Bandwidth: \(result.downlinkReport.availableBandwidth)Kbps"
        let downlinkJitter = "Downlink Jitter: \(result.downlinkReport.jitter)ms"
        let downlinkLoss = "Downlink Packet Loss Rate: \(result.downlinkReport.packetLossRate)%"
        let downlinkReport = [downlinkBandwidth, downlinkJitter, downlinkLoss].joined(separator: "\n")
        
        // Generate final probe result text
        let probeReport = [heading, "", "Uplink Report:", uplinkReport, "", "Downlink Report:", downlinkReport].joined(separator: "\n")
        print("probeReport: \(probeReport)")
        
        // Alert the final probe result
        let probeReportAlert = UIAlertController(title: "Network Quality Statistics", message: probeReport, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        probeReportAlert.addAction(okBtn)
        
        self.present(probeReportAlert, animated: true, completion: nil)
    }
}

